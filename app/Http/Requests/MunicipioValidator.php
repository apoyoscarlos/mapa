<?php

namespace App\Http\Requests;

use Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class MunicipioValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre'        => 'required|string|max:100',
            'abreviacion'   => 'required|string|max:5',
            'habitantes'    => 'required|integer',
            'enfermos'      => 'required|integer',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id'            => 'required|integer|exists:municipios,id',
            'nombre'        => 'required|string|max:100',
            'abreviacion'   => 'required|string|max:5',
            'habitantes'    => 'required|integer',
            'enfermos'      => 'required|integer',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
}
