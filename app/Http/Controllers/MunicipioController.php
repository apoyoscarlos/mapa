<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Municipio;
use App\Http\Controllers\Controller;
use App\Http\Requests\MunicipioValidator;
use Illuminate\Support\Facades\DB;
use Exception;

class MunicipioController extends Controller
{
    protected $validate;
    public function __construct(MunicipioValidator $validate)
    {
        $this->validate = $validate;
    }

    public function store(Request $request){
        $validate = $this->validate->store($request);
        $data = $request->all();

        if( $validate  !== true){
            return response()->json(['error'=> $validate->original], 403);
        }

        $item = Municipio::create($data);
        return response()->json(['id' => $item], 200);
    }

    public function update(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;

        try{
            $validacion = $this->validate->update($request);
            $data = $request->all();
           
            if( $validacion  !== true){
                return response()->json(['error'=> $validacion->original], 403);
            }

            $item = Municipio::where('id', $request->id)->update($data);
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->errorInfo[2];
        }
        return response()->json($response, $codigo);
    }

    public function destroy($id){
        $item = Municipio::find($id);
        if(is_null($item)){
            return response()->json( ['error'=> "The record was not found with id ".$item], 403);
        } 
        $item->delete();
        return response()->json(['success'=> true], 200);
    }

    public function get($id){
        $item = Municipio::find($id);
        if(is_null($item)){
            return response()->json( ['error'=> "The record was not found with id ".$id], 403);
        }
        return response()->json($item, 200);
    }

    public function find(Request $request){
        $items= [];
        $order = "desc";

        $query = DB::table('municipios');

        if(!is_null($request->abreviacion)){
            $query->where("abreviacion","=", $request->abreviacion);
        }
        if(!is_null($request->nombre)){
            $query->where("nombre","like", "%".$request->nombre."%");
        }
        if(!is_null($request->order)){
            $order = $request->order == 'asc'? "asc" : "desc";
        }

        $query->orderBy("id", $order);
        if(is_null($request->paginate) || $request->paginate == "true" ){
            $response = 10;
            if(!is_null($request->items_to_show)){
                $response = $request->items_to_show;
            }
            $items = $query->paginate($response); 
        }
        else{
            $items = $query->get();
            return response()->json(["data" => $items], 200);
        }
        return response()->json($items, 200);
    }
}
